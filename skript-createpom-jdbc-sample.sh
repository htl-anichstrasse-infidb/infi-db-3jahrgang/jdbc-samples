# steps to create and launch
# touch <skrip>.sh
# chmod +x <skrip>.sh
# nano <skrip>.sh
# edit <skrip>.sh
# execute <skrip>.sh
# ./skript.sh

# echo current date
date

# create mvn simple project

# DgroupId
# uniquely identifies your project across all projects.
# A group ID should follow Java's package name rules.
# This means it starts with a reversed domain name you control.

# DartifactId
# is the name of the jar without version. If you created it, then you can choose
# whatever name you want with lowercase letters and no strange symbols.
# If it's a third party jar, you have to take the name of the jar as it's distributed.

mvn -B archetype:generate \
-DarchetypeArtifactId=maven-archetype-quickstart \
-DarchetypeVersion=1.4 \
-DgroupId=at.htlinn.infi \
-DartifactId=jdbc-samples \
-Dpackage=at.htlinn.infi.J3.jdbc \
-Dversion=1.0-SNAPSHOT \
-DarchetypeCatalog=internal \
-DinteractiveMode=false
