# steps to create and launch
# touch <skrip>.sh
# chmod +x <skrip>.sh
# nano <skrip>.sh
# edit <skrip>.sh
# execute <skrip>.sh
# ./skript.sh
date

# pom.xml must be updated for each group this is done with sed !!!
# <artifactId>jdbc-samples</artifactId>
# <name>jdbc-samples</name>

# cd to template
cd /Users/markusamann/development/data/git/htl-anichstrasse-infidb/3Jahrgang/common/jdbc-samples
# 3A G1
cp -r jdbc-samples ../../2022/3ahwii/G1/jdbc-samples-3a-g1
cd ../../2022/3ahwii/G1/jdbc-samples-3a-g1
rm -r jdbc-samples-3a-g1
mv jdbc-samples jdbc-samples-3a-g1
cd jdbc-samples-3a-g1
sed -i '' s/\<artifactId\>jdbc-samples/\<artifactId\>jdbc-samples-3a-g1/g pom.xml
sed -i '' s/\<name\>jdbc-samples/\<name\>jdbc-samples-3a-g1/g pom.xml

# cd to template
cd /Users/markusamann/development/data/git/htl-anichstrasse-infidb/3Jahrgang/common/jdbc-samples
# 3A G2
cp -r jdbc-samples ../../2022/3ahwii/G2/jdbc-samples-3a-g2
cd ../../2022/3ahwii/G2/jdbc-samples-3a-g2
rm -r jdbc-samples-3a-g2
mv jdbc-samples jdbc-samples-3a-g2
cd jdbc-samples-3a-g2
sed -i '' s/\<artifactId\>jdbc-samples/\<artifactId\>jdbc-samples-3a-g2/g pom.xml
sed -i '' s/\<name\>jdbc-samples/\<name\>jdbc-samples-3a-g2/g pom.xml

# cd to template
cd /Users/markusamann/development/data/git/htl-anichstrasse-infidb/3Jahrgang/common/jdbc-samples
# 3B G1
cp -r jdbc-samples ../../2022/3bhwii/G1/jdbc-samples-3b-g1
cd ../../2022/3bhwii/G1/jdbc-samples-3b-g1
rm -r jdbc-samples-3b-g1
mv jdbc-samples jdbc-samples-3b-g1
cd jdbc-samples-3b-g1
sed -i '' s/\<artifactId\>jdbc-samples/\<artifactId\>jdbc-samples-3b-g1/g pom.xml
sed -i '' s/\<name\>jdbc-samples/\<name\>jdbc-samples-3b-g1/g pom.xml

# cd to template
cd /Users/markusamann/development/data/git/htl-anichstrasse-infidb/3Jahrgang/common/jdbc-samples
