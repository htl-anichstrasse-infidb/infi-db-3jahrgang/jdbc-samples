package at.htlinn.infi.J3.crypt;

import java.io.File;
import java.io.FileInputStream;
import java.io.PrintWriter;

public class CryptoIO {

  private CryptoStrategy strategy;
  private String text;

  public CryptoIO(CryptoStrategy startegy) {
    this.strategy = startegy;
  }

  public void writeFile(String fPath) {
    writeFile(fPath, strategy.encrypt(this.text));
  }

  public void encryptText() {
    this.text = strategy.encrypt(this.text);
  }

  public void read(String fPath) {
    this.text = strategy.decrypt(readFile(fPath));
  }

  public void decryptText() {
    this.text = strategy.decrypt(this.text);
  }

  public void setStrategy(CryptoStrategy strategy) {
    this.strategy = strategy;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  private String readFile(String fPath) {
    String cText = null;
    try {
      File file = new File(fPath);
      FileInputStream fis = new FileInputStream(file);
      byte[] data = new byte[(int) file.length()];
      fis.read(data);
      fis.close();
      cText = new String(data, "ASCII");
    } catch (Exception e) {
    }
    return cText;
  }

  private void writeFile(String fPath, String encText) {
    try {
      PrintWriter out = new PrintWriter(fPath);
      out.print(encText);
      out.close();
    } catch (Exception e) {
    }
  }
}
