package at.htlinn.infi.J3.itakademy.main;

import java.util.Optional;
import java.util.logging.Logger;
import at.htlinn.infi.J3.itakademy.dao.KursDao;
import at.htlinn.infi.J3.itakademy.impl.KursDaoFactory;
import at.htlinn.infi.J3.itakademy.model.Kurs;
import at.htlinn.infi.J3.jdbc.dao.DaoFactory;

public class AppItAkademy {
  static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

  public static void main(String[] args) {
    DaoFactory daoFactory = DaoFactory.create();
    KursDaoFactory kursDaoFactory = daoFactory.createKursDaoFactory();
    KursDao kursDao = kursDaoFactory.getKursDaoImpl();

    Optional<Kurs> kurs = kursDao.get("715eafd0-f282-11ed-a3c0-5caa40428aef");
    logger.info(kurs.toString());

    // return empty object
    logger.info(kursDao.get("715eafd0-f282-11ed-a3c0-xxxx").toString());
  }
}
