package at.htlinn.infi.J3.itakademy.model;

import java.sql.Timestamp;

public class Kurs {

  public static final class Builder {
    private String idKurs;
    private String nummer;
    private String titel;
    private Timestamp startDatum;
    private Integer dauer;

    public Builder setIdKurs(String idKurs) {
      this.idKurs = idKurs;
      return this;
    }

    public Builder setNummer(String nummer) {
      this.nummer = nummer;
      return this;
    }

    public Builder setTitel(String titel) {
      this.titel = titel;
      return this;
    }

    public Builder setStartDatum(Timestamp startDatum) {
      this.startDatum = startDatum;
      return this;
    }

    public Builder setDauer(Integer dauer) {
      this.dauer = dauer;
      return this;
    }

    public Kurs build() {
      return new Kurs(this);
    }
  }


  private String idKurs;
  private String nummer;
  private String titel;
  private Timestamp startDatum;
  private Integer dauer;

  public Kurs() {
    dauer = 0;
  }

  private Kurs(final Builder builder) {
    idKurs = builder.idKurs;
    nummer = builder.nummer;
    titel = builder.titel;
    startDatum = builder.startDatum;
    dauer = builder.dauer;
  }

  public String getIdKurs() {
    return idKurs;
  }

  public void setIdKurs(String idKurs) {
    this.idKurs = idKurs;
  }

  public String getNummer() {
    return nummer;
  }

  public void setNummer(String nummer) {
    this.nummer = nummer;
  }

  public String getTitel() {
    return titel;
  }

  public void setTitel(String titel) {
    this.titel = titel;
  }

  public Timestamp getStartDatum() {
    return startDatum;
  }

  public void setStartDatum(Timestamp startDatum) {
    this.startDatum = startDatum;
  }

  public Integer getDauer() {
    return dauer;
  }

  public void setDauer(Integer dauer) {
    this.dauer = dauer;
  }

  public String toString() {
    StringBuilder str = new StringBuilder();
    str.append("idKurs=").append(idKurs).append("; nummer=").append(nummer).append("; titel=")
        .append(titel).append("; startDatum=").append(startDatum).append("; dauer=").append(dauer);

    return str.toString();
  }
}
