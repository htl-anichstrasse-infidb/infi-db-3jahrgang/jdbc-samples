package at.htlinn.infi.J3.jdbc.main;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import at.htlinn.infi.J3.jdbc.db.DatabaseConnection;



public class App {
  static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

  public static void main(String[] args) {
    logger.setLevel(Level.ALL);
    logger.info("Start Test Client");
    App client = new App();
    client.process();
    logger.info("Test Client successfully terminated");
  }

  void process() {
    Connection con = null;
    try {
      con = DatabaseConnection.getConnection();
      con.setAutoCommit(true);
      PreparedStatement pstmt = con.prepareStatement(createSqlSelectAll());
      ResultSet rs = pstmt.executeQuery();
      ResultSetMetaData meta = rs.getMetaData();
      int columnCount = meta.getColumnCount();
      while (rs.next()) {
        StringBuilder out = new StringBuilder();
        for (int iCol = 1; iCol <= columnCount; iCol++) {
          String name = meta.getColumnLabel(iCol);
          String value = typeConverter(rs, iCol).toString();
          out.append(name).append("=").append(value).append(";");
        }
        logger.info(out.toString());
      }
    } catch (SQLException e) {
      traceSqlException(e);
      e.printStackTrace();
    } finally {
      try {
        DatabaseConnection.close();
      } catch (SQLException e) {
        traceSqlException(e);
        e.printStackTrace();
      }
    }
  }

  private Object typeConverter(ResultSet rs, int iCol) throws SQLException {
    ResultSetMetaData meta = rs.getMetaData();
    int type = meta.getColumnType(iCol);

    Object obj = null;
    switch (type) {
      case Types.CHAR:
      case Types.VARCHAR:
        obj = rs.getString(iCol);
        break;
      case Types.SMALLINT:
        break;
      case Types.INTEGER:
        obj = Integer.toString(rs.getInt(iCol));
      case Types.BIGINT:
        // TODO implement
        break;
      case Types.DECIMAL:
        // TODO implement
        break;
      case Types.NUMERIC:
        // TODO implement
        break;
      case Types.FLOAT:
        // TODO implement
        break;
      case Types.DOUBLE:
        // TODO implement
        break;
      case Types.DATE:
        // TODO implement
        break;
      case Types.TIMESTAMP:
        java.util.Calendar cal = Calendar.getInstance();
        cal.setTimeZone(TimeZone.getTimeZone("UTC"));
        java.sql.Timestamp ts = rs.getTimestamp(iCol);
        cal.setTime(ts);

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss z");
        sdf.setTimeZone(TimeZone.getTimeZone("CET"));

        obj = sdf.format(cal.getTime());
      case Types.TIMESTAMP_WITH_TIMEZONE:
        break;
    }

    if (rs.wasNull() || obj == null) {
      return "<null>";
    } else {
      return obj;
    }
  }

  private void traceSqlException(SQLException e) {
    logger.severe("SQLException: " + e.getMessage());
    logger.severe("SQLState: " + e.getSQLState());
    logger.severe("VendorError: " + e.getErrorCode());
  }

  private String createSqlSelectAll() {
    StringBuilder sqlStmt = new StringBuilder();
    sqlStmt.append("SELECT KU.idKurs,KU.nummer,KU.dauer,KU.titel,KU.startDatum")
        .append(",TN.nummer,TN.vorname,TN.nachname,TN.telNummer,TN.fachgebiet,TN.typ")
        .append(" FROM").append(" Kurs KU INNER JOIN Kurs_has_Teilnehmer KUTN")
        .append(" ON KU.idKurs = KUTN.idKurs").append(" INNER JOIN Teilnehmer TN")
        .append(" ON TN.idTeilnehmer = KUTN.idTeilnehmer");
    return sqlStmt.toString();
  }
}
