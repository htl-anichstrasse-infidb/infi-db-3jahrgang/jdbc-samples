package at.htlinn.infi.J3.itakademy.dao;

import java.util.Optional;
import java.util.Set;
import at.htlinn.infi.J3.itakademy.model.Kurs;
import at.htlinn.infi.J3.jdbc.dao.Dao;

public interface KursDao extends Dao<Kurs> {
  public Set<Kurs> getKursByNummer(Optional<Kurs> kurs);
}
