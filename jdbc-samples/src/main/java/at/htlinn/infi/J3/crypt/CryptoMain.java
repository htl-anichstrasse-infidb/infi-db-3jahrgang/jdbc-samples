package at.htlinn.infi.J3.crypt;

import java.io.FileReader;
import java.util.Properties;

public class CryptoMain {

  private static final String CRED_PATHFILENAME = "resources/credentials.properties";
  private static final String CRED_PRIVATE_KEY_PATH = "privateKeyPath";
  private static final String CRED_PRIVATE_KEY_FILENAME = "privateKeyFilename";
  private static final String PK = "privateKey";
  private static final String PK_PW_ROOT = "pwMySqlRoot";

  public static void main(String args[]) throws Exception {

    FileReader readerCredentials = new FileReader(CRED_PATHFILENAME);
    Properties propCredentials = new Properties();
    propCredentials.load(readerCredentials);

    String privateKeyPath = propCredentials.getProperty(CRED_PRIVATE_KEY_PATH);
    String privateKeyFilename = propCredentials.getProperty(CRED_PRIVATE_KEY_FILENAME);

    FileReader readerPrivateKey = new FileReader(privateKeyPath + "/" + privateKeyFilename);
    Properties propPrivateKey = new Properties();
    propPrivateKey.load(readerPrivateKey);

    CryptoStrategy xorStrategy = new Xor(propPrivateKey.getProperty(PK));
    CryptoIO crypto = new CryptoIO(xorStrategy);
    crypto.setStrategy(xorStrategy);

    crypto.setText(propPrivateKey.getProperty(PK_PW_ROOT));
    crypto.encryptText();
    System.out.println("<" + crypto.getText() + ">");
  }
}
