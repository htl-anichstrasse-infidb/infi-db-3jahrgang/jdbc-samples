package at.htlinn.infi.J3.jdbc.db;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import at.htlinn.infi.J3.crypt.CryptoIO;
import at.htlinn.infi.J3.crypt.CryptoStrategy;
import at.htlinn.infi.J3.crypt.Xor;

public class DbConfig {
  private String userID;
  private String pw;
  private String url;
  private String dbName;
  private String serverTimeZone;
  private boolean allowPublicKeyRetrieval;
  private String driverNameMySql;

  private static DbConfig dbConfig = null;

  private static final String CRED_PATHFILENAME = "resources/credentials.properties";
  private static final String CRED_PRIVATE_KEY_PATH = "privateKeyPath";
  private static final String CRED_PRIVATE_KEY_FILENAME = "privateKeyFilename";
  private static final String PK = "privateKey";
  private static final String PK_PW_ROOT = "pwRoot";

  private Properties propConfig;

  private DbConfig() {
    allowPublicKeyRetrieval = false;
  }

  public static DbConfig create() {
    if (dbConfig == null) {
      dbConfig = new DbConfig();
      dbConfig.init();
    }
    return dbConfig;
  }

  private void init() {
    FileReader readerConfig = null;
    try {
      readerConfig = new FileReader("resources/config.properties");
      propConfig = new Properties();
      propConfig.load(readerConfig);

      url = propConfig.getProperty("url");
      dbName = propConfig.getProperty("dbName");
      serverTimeZone = propConfig.getProperty("serverTimeZone");
      allowPublicKeyRetrieval =
          propConfig.getProperty("allowPublicKeyRetrieval").equalsIgnoreCase("true") ? true : false;
      driverNameMySql = propConfig.getProperty("driverNameMySql");


      FileReader readerCredentials = new FileReader(CRED_PATHFILENAME);
      Properties propCredentials = new Properties();
      propCredentials.load(readerCredentials);

      String privateKeyPath = propCredentials.getProperty(CRED_PRIVATE_KEY_PATH);
      String privateKeyFilename = propCredentials.getProperty(CRED_PRIVATE_KEY_FILENAME);

      FileReader readerPrivateKey = new FileReader(privateKeyPath + "/" + privateKeyFilename);
      Properties propPrivateKey = new Properties();
      propPrivateKey.load(readerPrivateKey);

      CryptoStrategy xorStrategy = new Xor(propPrivateKey.getProperty(PK));
      CryptoIO crypto = new CryptoIO(xorStrategy);
      crypto.setStrategy(xorStrategy);

      crypto.setText(propCredentials.getProperty(PK_PW_ROOT));
      crypto.decryptText();
      pw = crypto.getText();

      userID = propCredentials.getProperty("useridRoot");


    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public String getDriverNameMySql() {
    return driverNameMySql;
  }

  public String getDbName() {
    return dbName;
  }

  public String getConfigPropertyByName(String name) {
    return propConfig.getProperty(name);
  }

  public String getServerTimeZone() {
    return serverTimeZone;
  }

  public boolean isAllowPublicKeyRetrieval() {
    return allowPublicKeyRetrieval;
  }

  public String getAllowPublicKeyRetrieval() {
    return allowPublicKeyRetrieval == true ? "true" : "false";
  }

  public String getUserID() {
    return userID;
  }

  public String getPw() {
    return pw;
  }

  public String getUrl() {
    return url;
  }
}
