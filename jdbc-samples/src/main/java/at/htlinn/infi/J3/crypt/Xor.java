package at.htlinn.infi.J3.crypt;

public class Xor implements CryptoStrategy {

  private String privateKey;

  public Xor(String privatKey) {
    privateKey = privatKey;
  }

  @Override
  public String encrypt(String s) {
    // verschlüsseln
    String encText = "";

    for (int i = 0, k = 0; i < s.length(); i++, k++) {
      char ch = s.charAt(i);
      if (k >= privateKey.length()) {
        k = 0;
      }
      char charKey = privateKey.charAt(k);
      int result = (int) ch ^ (int) charKey;

      String intAsHex = "00" + Integer.toHexString(result);
      intAsHex = intAsHex.substring(intAsHex.length() - 2, intAsHex.length());

      encText += intAsHex;
    }
    return encText;
  }

  // "1a1ce137202c585c"

  @Override
  public String decrypt(String s) {
    // entschlüsseln
    String decText = "";
    for (int i = 0, k = 0; i < s.length(); i += 2, k++) {
      String hexStringofChar = s.substring(i, i + 2);
      hexStringofChar = "0x" + hexStringofChar;
      int intValueOfChar = Integer.decode(hexStringofChar);

      if (k >= privateKey.length()) {
        k = 0;
      }
      char charKey = privateKey.charAt(k);
      int decryptedChar = intValueOfChar ^ (int) charKey;

      char value = (char) decryptedChar;
      decText += value;
    }

    return decText;
  }
}
