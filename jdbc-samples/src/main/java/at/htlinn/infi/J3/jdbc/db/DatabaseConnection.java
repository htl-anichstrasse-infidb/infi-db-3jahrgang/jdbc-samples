package at.htlinn.infi.J3.jdbc.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Logger;

public class DatabaseConnection {
  static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

  private static DatabaseConnection databaseConnection = null;
  private static Connection con;

  private DatabaseConnection() {
    connect();
  }

  private void connect() {
    logger.info("start database connection");

    DbConfig dbConfig = DbConfig.create();

    try {
      Class.forName(dbConfig.getDriverNameMySql());
      StringBuilder url = new StringBuilder();
      url.append(dbConfig.getUrl()).append("/").append(dbConfig.getDbName()).append("?")
          .append("user=").append(dbConfig.getUserID()).append("&").append("password=")
          .append(dbConfig.getPw()).append("&").append("serverTimezone=")
          .append(dbConfig.getServerTimeZone()).append("&").append("allowPublicKeyRetrieval=")
          .append(dbConfig.getAllowPublicKeyRetrieval()).append("");

      con = DriverManager.getConnection(url.toString());
      logger.info("database is connected: DbName=" + dbConfig.getDbName());
    } catch (SQLException e) {
      traceSqlException(e);
      e.printStackTrace();
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
  }

  public static void close() throws SQLException {
    con.close();
    databaseConnection = null;
  }

  private void traceSqlException(SQLException e) {
    logger.severe("SQLException: " + e.getMessage());
    logger.severe("SQLState: " + e.getSQLState());
    logger.severe("VendorError: " + e.getErrorCode());
  }

  public static Connection getConnection() {
    if (databaseConnection == null) {
      databaseConnection = new DatabaseConnection();
    }
    return con;
  }
}
