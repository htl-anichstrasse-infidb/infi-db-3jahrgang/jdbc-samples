package at.htlinn.infi.J3.itakademy.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import at.htlinn.infi.J3.itakademy.dao.KursDao;
import at.htlinn.infi.J3.itakademy.model.Kurs;
import at.htlinn.infi.J3.jdbc.db.DatabaseConnection;

public class KursDaoImpl implements KursDao {

  private static final Logger LOGGER = Logger.getLogger("KursDaoImpl");
  private final Connection connection = DatabaseConnection.getConnection();

  public KursDaoImpl() {
    LOGGER.setLevel(Level.INFO);
  }

  @Override
  public Optional<Kurs> get(String id) {
    Kurs kurs = null;
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT idKurs,startDatum,dauer,titel,nummer").append(" FROM Kurs")
        .append(" WHERE idKurs = ?");

    try {
      PreparedStatement pstmt = connection.prepareStatement(sql.toString());
      pstmt.setString(1, id);
      ResultSet rs = pstmt.executeQuery();

      if (!rs.isBeforeFirst()) {
        LOGGER.info("no data row");
      }

      int count = 0;
      while (rs.next()) {
        count++;
        if (count > 1) {
          LOGGER.info("to many rows");
          continue;
        }
        // SELECT idKurs, startDatum, dauer, titel, nummer

        Timestamp startDatum = rs.getTimestamp(2);
        int dauer = rs.getInt(3);
        String titel = rs.getString(4);
        String nummer = rs.getString(5);

        kurs = new Kurs.Builder().setIdKurs(id).setDauer(dauer).setNummer(nummer)
            .setStartDatum(startDatum).setTitel(titel).build();
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }

    return Optional.ofNullable(kurs);
  }

  @Override
  public List<Kurs> getAll() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public void save(Kurs t) {
    // TODO Auto-generated method stub
  }

  @Override
  public void update(Kurs t, String[] params) {
    // TODO Auto-generated method stub
  }

  @Override
  public void delete(Kurs t) {
    // TODO Auto-generated method stub
  }

  @Override
  public Set<Kurs> getKursByNummer(Optional<Kurs> kurs) {
    // TODO Auto-generated method stub
    return null;
  }
}
