package at.htlinn.infi.J3.crypt;

public interface CryptoStrategy {

  public String encrypt(String s);

  public String decrypt(String s);

}
