package at.htlinn.infi.J3.jdbc.dao;

import at.htlinn.infi.J3.itakademy.impl.KursDaoFactory;

public class DaoFactory {

  public static DaoFactory create() {
    return new DaoFactory();
  }

  public KursDaoFactory createKursDaoFactory() {
    return new KursDaoFactory();
  }
}
