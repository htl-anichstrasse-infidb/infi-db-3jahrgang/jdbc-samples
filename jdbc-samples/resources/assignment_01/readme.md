# Assignment 01
```
Implement entity-type Teilnehmer.
```

# General information
```
Work on the following subtasks in the given order.
First of all pull new provided stuff from central repository and merge into your
private branch. Work only on your private branch and don't modify provided files.
```

### Task 1: Create new package in your existing Project jdbc-samples-class-group
```
eclipse -> project -> New -> Package (Java Package)
Source Folder: ../src/main/java
Name: at.htlinn.infi.J3.itakademy.assignment01
```

### Task 2: Implement class Teilnehmer
```
eclipse -> project -> New -> Class (Java class)
Source Folder: ../src/main/java
Package: at.htlinn.infi.J3.itakademy.assignment01
Name: Teilnehmer
```

### Task 3: Implement class TeilnehmerDaoFactory
```
eclipse -> project -> New -> Class (Java class)
Source Folder: ../src/main/java
Package: at.htlinn.infi.J3.itakademy.assignment01
Class Name: TeilnehmerDaoFactory
```

### Task 3.1: Implement interface TeilnehmerDao extending Dao<Teilnehmer>
```
eclipse -> project -> New -> Interface (Java interface)
Source Folder: ../src/main/java
Package: at.htlinn.infi.J3.itakademy.assignment01
Interface Name: KursDao
```

### Task 4: Implement class TeilnehmerDaoImpl
```
eclipse -> project -> New -> Class (Java class)
Source Folder: ../src/main/java
Package: at.htlinn.infi.J3.itakademy.assignment01
Class Name: TeilnehmerDaoImpl
```

### Task 4.1: Implement Member public Optional<Teilnehmer> get(String id)
```
Package: at.htlinn.infi.J3.itakademy.assignment01
Class Name: TeilnehmerDaoImpl
Method Name: public Optional<Teilnehmer> get(String id)
```

### Task 4.2: [optional] Implement Member public List<Teilnehmer> getAll()
```
Package: at.htlinn.infi.J3.itakademy.assignment01
Class Name: TeilnehmerDaoImpl
Method Name: public List<Teilnehmer> getAll()
```

### Task 4.3: [optional] Implement Member public void save(Teilnehmer t)
```
Package: at.htlinn.infi.J3.itakademy.assignment01
Class Name: TeilnehmerDaoImpl
Method Name: public List<Teilnehmer> getAll()
```

### Task 4.4: [optional] Implement Member public void update(Teilnehmer t, String[] params)
```
Package: at.htlinn.infi.J3.itakademy.assignment01
Class Name: TeilnehmerDaoImpl
Method Name: public void update(Teilnehmer t, String[] params)
```

### Task 4.5: [optional] Implement Member public void delete(Teilnehmer t)
```
Package: at.htlinn.infi.J3.itakademy.assignment01
Class Name: TeilnehmerDaoImpl
Method Name: public void delete(Teilnehmer t)
```

### Task 4.6: [optional] Implement Member public Set<Kurs> getKursByNummer(Optional<Teilnehmer> teilnehmer)
```
Package: at.htlinn.infi.J3.itakademy.assignment01
Class Name: TeilnehmerDaoImpl
Method Name: public Set<Kurs> getKursByNummer(Optional<Teilnehmer> teilnehmer)
```

### Task 5: Implement class TestTeilnehmer with Member main() to test the Function from Task 4.1
```
eclipse -> project -> New -> Class (Java class)
Source Folder: ../src/main/java
Package: at.htlinn.infi.J3.itakademy.assignment01
Class Name: TestTeilnehmer
Method: main
```
