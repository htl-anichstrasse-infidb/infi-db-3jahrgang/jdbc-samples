-- 1.) CREATE Database structure
DROP DATABASE IF EXISTS ITAkademyV001;
CREATE DATABASE ITAkademyV001;
USE ITAkademyV001;

-- Kurs definition
CREATE TABLE `Kurs` (
  `idKurs` char(36) NOT NULL,
  `startDatum` datetime NOT NULL,
  `dauer` int(10) unsigned NOT NULL,
  `titel` varchar(255) DEFAULT NULL,
  `nummer` varchar(100) NOT NULL,
  PRIMARY KEY (`idKurs`),
  UNIQUE KEY `kurs_nummer_unique` (`nummer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Teilnehmer definition
CREATE TABLE `Teilnehmer` (
  `idTeilnehmer` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `vorname` varchar(255) DEFAULT NULL,
  `nachname` varchar(255) DEFAULT NULL,
  `telNummer` varchar(55) DEFAULT NULL,
  `fachgebiet` varchar(255) DEFAULT NULL,
  `typ` char(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `nummer` varchar(100) NOT NULL,
  PRIMARY KEY (`idTeilnehmer`),
  UNIQUE KEY `teilnehmer_nummer_unique` (`nummer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Kurs_has_Teilnehmer definition
CREATE TABLE `Kurs_has_Teilnehmer` (
  `idKursHasTeilnehmer` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `idKurs` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `idTeilnehmer` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`idKursHasTeilnehmer`),
  KEY `fk_Kurs_has_KursTeilnehmer_Teilnehmer_idx` (`idTeilnehmer`),
  KEY `fk_Kurs_has_KursTeilnehmer_Kurs_idx` (`idKurs`),
  CONSTRAINT `fk_Kurs_has_KursTeilnehmer_Kurs` FOREIGN KEY (`idKurs`) REFERENCES `kurs` (`idkurs`),
  CONSTRAINT `fk_Kurs_has_KursTeilnehmer_Teilnehmer` FOREIGN KEY (`idTeilnehmer`) REFERENCES `teilnehmer` (`idTeilnehmer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
