INSERT INTO Kurs (idKurs, startDatum, dauer, titel, nummer)
     VALUES (UUID(), '2022-05-09 00:00:00', 60, 'Einführung JDBC', 'INFI100')
;
INSERT INTO Teilnehmer (idTeilnehmer, vorname, nachname, telNummer, fachgebiet, typ, nummer)
     VALUES (UUID(), 'Harald', 'Meier', '+43xxxYYYYYYYY', NULL, NULL, 'PERN001')
;
INSERT INTO Teilnehmer (idTeilnehmer, vorname, nachname, telNummer, fachgebiet, typ, nummer)
     VALUES (UUID(), 'Werner', 'Maier', '+43xxxYYYYYYYY', NULL, NULL, 'PERN002')
;
INSERT INTO Kurs_has_Teilnehmer (idKursHasTeilnehmer, idKurs, idTeilnehmer)
SELECT UUID(), idKurs, idTeilnehmer
  FROM Kurs KU INNER JOIN Teilnehmer TN
    ON KU.nummer = 'INFI100'
   AND TN.nummer = 'PERN001'
;
INSERT INTO Kurs_has_Teilnehmer (idKursHasTeilnehmer, idKurs, idTeilnehmer)
SELECT UUID(), idKurs, idTeilnehmer
  FROM Kurs KU INNER JOIN Teilnehmer TN
    ON KU.nummer = 'INFI100'
   AND TN.nummer = 'PERN002'
;

-- select all
SELECT KU.idKurs, KU.nummer, KU.dauer, KU.titel, KU.startDatum
      ,TN.nummer, TN.vorname, TN.nachname, TN.telNummer, TN.fachgebiet, TN.typ 
  FROM Kurs KU
 INNER JOIN Kurs_has_Teilnehmer KUTN
    ON KU.idKurs = KUTN.idKurs
 INNER JOIN Teilnehmer TN
    ON TN.idTeilnehmer = KUTN.idTeilnehmer
 ;
 
 -- KursDaoImpl -> Optional<Kurs> get(String id)
SELECT idKurs, startDatum, dauer, titel, nummer 
  FROM Kurs
 WHERE idKurs = '715eafd0-f282-11ed-a3c0-5caa40428aef'
;

